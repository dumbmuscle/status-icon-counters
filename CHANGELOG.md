2.0.12
- Improved font scaling when not using the default grid size.
- The rebind keys option will now also bind numpad digits.
- Fixed issues when creating or removing countdowns.
- Added Japanese translation by BrotherSharper.

2.0.11
- Improved compatibility with Status Icon Tweaks.
- Restored compatibility with some other modules that change status effects. Note that some of them require the libWrapper module.
- Fixed Mac command key not working for counter type selection.

2.0.10
- Added compatibility with Status Icon Tweaks.
- Fixed an issue when the system uses active effects without icons.

2.0.9
- Fixed number keys not working.

2.0.8
- Fixed counter rendering for 0.9.

2.0.7
- Updated for FoundryVTT 0.9.
- Fixed overlay for active effects.
- Fixed an issue that could randomly prevent canvas rendering.
- Added API method to clear all effects of a token.

2.0.6
- Fixed counters of unlinked tokens disappearing when switching scenes.
- Fixed toggling the overlay effect.

2.0.5
- Updated for FoundryVTT 0.8. This release will not work with older versions.
- Changed API methods to use TokenDocument instead of Token.
- Added display value function for custom types.
- Fixed toggling of overlay effects.

2.0.4
- Adjusted duplicate prevention to be more compatible with modules doing it intentionally.
- Made effects modifiable by reading them from the collection instead of the source data.

2.0.3
- Fixed duration calculation for Foundry 0.7.9. **Note**: This breaks backward compatibility, so all prior versions are no longer supported.
- Countdowns are no longer reduced by 1 when the combat starts.
- Round based countdowns now respect the starting turn (rather than decrementing at the start of the round). Let me know if anyone needs a setting to restore the old behavior.

2.0.2
- Fixed a harmless error when other modules replace the token HUD.
- Forward mouse events (e.g. hover for tooltips) to the status icon.
- Added a missing setting translation.
- Added support for conflict detection using [libWrapper](https://github.com/ruipin/fvtt-lib-wrapper).
- API only: Allow custom types to prevent updates by setting `forceUpdate` to `false` explicitly.

2.0.1
- Fixed counter rendering for linked tokens.
- Fixed countdown for linked tokens.

2.0.0
- Added an option when to display '1' on the counter.
- Counters are now displayed in the combat tracker.
- Effects can now have different types. Press *Ctrl + Left click* in the token HUD to select the type.
- Added countdown type which sets the duration of active effects.
- Added multiplier type which changes the value of numeric active effects.
- Improved resiliency for changes made by other modules and systems.
- Significantly changed and expanded the API for developers.
- **Note**: This version will reset your existing counters.

1.0.2
- Fixed issue for custom icons from URL sources.
- **Note**: This version will reset your existing counters.

1.0.1
- Fixed compatibility issue with modules using PIXI's default texture loading.

1.0.0
- Core functionality.