<a name="module_CounterAPI"></a>

## CounterAPI
Provides methods for accessing and manipulating status effect counters. The included classes are added to the window to allow using them as an optional dependency which can be resolved after the Foundry module has been loaded.


* [CounterAPI](#module_CounterAPI)
    * [~EffectCounter](#module_CounterAPI..EffectCounter)
        * [new EffectCounter(value, path, tokenDoc, delayAssignment)](#new_module_CounterAPI..EffectCounter_new)
        * _instance_
            * [.font](#module_CounterAPI..EffectCounter+font) ⇒ <code>PIXI.TextStyle</code>
            * [.findParent([parent])](#module_CounterAPI..EffectCounter+findParent) ⇒ <code>TokenDocument</code>
            * [.getValue([parent])](#module_CounterAPI..EffectCounter+getValue) ⇒ <code>Number</code>
            * [.getDisplayValue([parent])](#module_CounterAPI..EffectCounter+getDisplayValue) ⇒ <code>String</code>
            * [.setValue(value, [parent], [forceUpdate])](#module_CounterAPI..EffectCounter+setValue) ⇒ <code>Promise</code>
            * [.getScaledFont([iconHeight])](#module_CounterAPI..EffectCounter+getScaledFont) ⇒ <code>PIXI.TextStyle</code>
            * [.allowType(type, [parent])](#module_CounterAPI..EffectCounter+allowType) ⇒ <code>boolean</code>
            * [.getDefaultType([parent])](#module_CounterAPI..EffectCounter+getDefaultType) ⇒ <code>string</code>
            * [.changeType(type, [parent])](#module_CounterAPI..EffectCounter+changeType) ⇒ <code>Promise</code>
            * [.update([parent])](#module_CounterAPI..EffectCounter+update) ⇒ <code>Promise</code>
            * [.remove([parent])](#module_CounterAPI..EffectCounter+remove) ⇒ <code>Promise</code>
        * _static_
            * [.findCounter(tokenDoc, iconPath)](#module_CounterAPI..EffectCounter.findCounter) ⇒ <code>EffectCounter</code>
            * [.findCounterValue(tokenDoc, iconPath)](#module_CounterAPI..EffectCounter.findCounterValue) ⇒ <code>number</code>
            * [.getCounters(tokenDoc)](#module_CounterAPI..EffectCounter.getCounters) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
            * [.getAllCounters(token)](#module_CounterAPI..EffectCounter.getAllCounters) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
            * [.clearEffects(tokenDoc)](#module_CounterAPI..EffectCounter.clearEffects) ⇒ <code>Promise</code>
            * [.redrawCounters(token, counters)](#module_CounterAPI..EffectCounter.redrawCounters)
            * [.drawCounters()](#module_CounterAPI..EffectCounter.drawCounters)
    * [~ActiveEffectCounter](#module_CounterAPI..ActiveEffectCounter) ⇐ <code>EffectCounter</code>
        * [new ActiveEffectCounter(value, path, parent)](#new_module_CounterAPI..ActiveEffectCounter_new)
        * _instance_
            * [.findParent([parent])](#module_CounterAPI..ActiveEffectCounter+findParent) ⇒ <code>ActiveEffect</code>
            * [.update([parent])](#module_CounterAPI..ActiveEffectCounter+update) ⇒ <code>Promise</code>
            * [.remove([parent])](#module_CounterAPI..ActiveEffectCounter+remove) ⇒ <code>Promise</code>
        * _static_
            * [.findCounter(actor, statusId)](#module_CounterAPI..ActiveEffectCounter.findCounter) ⇒ <code>ActiveEffectCounter</code>
            * [.findCounterValue(actor, statusId)](#module_CounterAPI..ActiveEffectCounter.findCounterValue) ⇒ <code>number</code>
            * [.getCounters(parent)](#module_CounterAPI..ActiveEffectCounter.getCounters) ⇒ <code>Array.&lt;ActiveEffectCounter&gt;</code>
    * [~CounterTypes](#module_CounterAPI..CounterTypes)
        * [.types](#module_CounterAPI..CounterTypes.types) ⇒ <code>Object</code>
        * [.defaults](#module_CounterAPI..CounterTypes.defaults) ⇒ <code>Object</code>
        * [.addType(type, [valueGetter], [valueSetter], [allowSelector], [valueDisplay])](#module_CounterAPI..CounterTypes.addType)
        * [.setFont(type, font)](#module_CounterAPI..CounterTypes.setFont)
        * [.setDefaultType(path, type)](#module_CounterAPI..CounterTypes.setDefaultType)

<a name="module_CounterAPI..EffectCounter"></a>

### CounterAPI~EffectCounter
Represents a counter for a single status effect.

**Kind**: inner class of [<code>CounterAPI</code>](#module_CounterAPI)  

* [~EffectCounter](#module_CounterAPI..EffectCounter)
    * [new EffectCounter(value, path, tokenDoc, delayAssignment)](#new_module_CounterAPI..EffectCounter_new)
    * _instance_
        * [.font](#module_CounterAPI..EffectCounter+font) ⇒ <code>PIXI.TextStyle</code>
        * [.findParent([parent])](#module_CounterAPI..EffectCounter+findParent) ⇒ <code>TokenDocument</code>
        * [.getValue([parent])](#module_CounterAPI..EffectCounter+getValue) ⇒ <code>Number</code>
        * [.getDisplayValue([parent])](#module_CounterAPI..EffectCounter+getDisplayValue) ⇒ <code>String</code>
        * [.setValue(value, [parent], [forceUpdate])](#module_CounterAPI..EffectCounter+setValue) ⇒ <code>Promise</code>
        * [.getScaledFont([iconHeight])](#module_CounterAPI..EffectCounter+getScaledFont) ⇒ <code>PIXI.TextStyle</code>
        * [.allowType(type, [parent])](#module_CounterAPI..EffectCounter+allowType) ⇒ <code>boolean</code>
        * [.getDefaultType([parent])](#module_CounterAPI..EffectCounter+getDefaultType) ⇒ <code>string</code>
        * [.changeType(type, [parent])](#module_CounterAPI..EffectCounter+changeType) ⇒ <code>Promise</code>
        * [.update([parent])](#module_CounterAPI..EffectCounter+update) ⇒ <code>Promise</code>
        * [.remove([parent])](#module_CounterAPI..EffectCounter+remove) ⇒ <code>Promise</code>
    * _static_
        * [.findCounter(tokenDoc, iconPath)](#module_CounterAPI..EffectCounter.findCounter) ⇒ <code>EffectCounter</code>
        * [.findCounterValue(tokenDoc, iconPath)](#module_CounterAPI..EffectCounter.findCounterValue) ⇒ <code>number</code>
        * [.getCounters(tokenDoc)](#module_CounterAPI..EffectCounter.getCounters) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
        * [.getAllCounters(token)](#module_CounterAPI..EffectCounter.getAllCounters) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
        * [.clearEffects(tokenDoc)](#module_CounterAPI..EffectCounter.clearEffects) ⇒ <code>Promise</code>
        * [.redrawCounters(token, counters)](#module_CounterAPI..EffectCounter.redrawCounters)
        * [.drawCounters()](#module_CounterAPI..EffectCounter.drawCounters)

<a name="new_module_CounterAPI..EffectCounter_new"></a>

#### new EffectCounter(value, path, tokenDoc, delayAssignment)
Initializes this counter.


| Param | Type | Description |
| --- | --- | --- |
| value | <code>Object</code> \| <code>number</code> | The initial counter value. For numbers, the counter is  initialized as a simple counter. For an object, its properties are  copied to the new EffectCounter and missing values are defaulted. |
| path | <code>string</code> | The icon path of the counter. |
| tokenDoc | <code>TokenDocument</code> | The token document that the counter is associated with. |
| delayAssignment | <code>boolean</code> | Internal flag to prevent the counter from immediately  initializing its type and count. |

<a name="module_CounterAPI..EffectCounter+font"></a>

#### effectCounter.font ⇒ <code>PIXI.TextStyle</code>
Retreives the font associated with the type of this counter. If none is  found, the default font is returned instead.

**Kind**: instance property of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>PIXI.TextStyle</code> - The font to use for this counter.  
<a name="module_CounterAPI..EffectCounter+findParent"></a>

#### effectCounter.findParent([parent]) ⇒ <code>TokenDocument</code>
Resolves the parent entity of this counter. If the parameter is omitted or is not a valid token, the token is searched with the stored token ID.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>TokenDocument</code> - The parent token document of the counter.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> | The parent entity of this counter.  If omitted, it will be resolved using the token ID. |

<a name="module_CounterAPI..EffectCounter+getValue"></a>

#### effectCounter.getValue([parent]) ⇒ <code>Number</code>
Retreives the value of this counter using its type's getter function.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Number</code> - The result of the counter's type's value getter.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> \| <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+getDisplayValue"></a>

#### effectCounter.getDisplayValue([parent]) ⇒ <code>String</code>
Retreives the value that should be displayed when rendering the counter.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>String</code> - The counter value if it is visible, an empty string otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> \| <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+setValue"></a>

#### effectCounter.setValue(value, [parent], [forceUpdate]) ⇒ <code>Promise</code>
Modifies the value of this counter and updates its visibility using its type's setter function.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>number</code> | The value to set. |
| [parent] | <code>TokenDocument</code> \| <code>ActiveEffect</code> | The parent entity of this counter. |
| [forceUpdate] | <code>boolean</code> | Flag indicating that an update should be performed regardless  of the setters return value. A value of false will prevent any update. |

<a name="module_CounterAPI..EffectCounter+getScaledFont"></a>

#### effectCounter.getScaledFont([iconHeight]) ⇒ <code>PIXI.TextStyle</code>
Creates a copy of the font associated with the type of this counter or the default, scaled relative to the given icon size.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>PIXI.TextStyle</code> - The scaled font to use for this counter and icon size.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [iconHeight] | <code>number</code> | <code>20</code> | The height of the effect icon in pixels. Defaults to 20. |

<a name="module_CounterAPI..EffectCounter+allowType"></a>

#### effectCounter.allowType(type, [parent]) ⇒ <code>boolean</code>
Calls the selector associated with the given type to determine whether that type may be selected in the status context menu.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>boolean</code> - False if the type does not exist, true if the selector is not set, the return value of the selector otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | The target type to check against. |
| [parent] | <code>TokenDocument</code> \| <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+getDefaultType"></a>

#### effectCounter.getDefaultType([parent]) ⇒ <code>string</code>
Resolves the default type of this counter using its icon path.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>string</code> - The name of the counter's default type.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+changeType"></a>

#### effectCounter.changeType(type, [parent]) ⇒ <code>Promise</code>
Modifies the type of the counter and resets its value using the old getter and the new setter.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | The new type of the counter. |
| [parent] | <code>TokenDocument</code> \| <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+update"></a>

#### effectCounter.update([parent]) ⇒ <code>Promise</code>
Adds or replaces this counter for the given token. This function always updates without checking if any changes occured.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter+remove"></a>

#### effectCounter.remove([parent]) ⇒ <code>Promise</code>
Removes this counter and its effect from its token.

**Kind**: instance method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> | The parent entity of this counter. |

<a name="module_CounterAPI..EffectCounter.findCounter"></a>

#### EffectCounter.findCounter(tokenDoc, iconPath) ⇒ <code>EffectCounter</code>
Retreives the counter for the effect with the given icon path for the given token.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>EffectCounter</code> - The counter object if it exists, undefined otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| tokenDoc | <code>TokenDocument</code> | The token document to search for the path. |
| iconPath | <code>string</code> | The icon path of the effect to search the counter for. |

<a name="module_CounterAPI..EffectCounter.findCounterValue"></a>

#### EffectCounter.findCounterValue(tokenDoc, iconPath) ⇒ <code>number</code>
Retreives the value of the counter for the effect with the given icon path for the given token.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>number</code> - The value of the counter if it exists, undefined otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| tokenDoc | <code>TokenDocument</code> | The token document to search for the path. |
| iconPath | <code>string</code> | The icon path of the effect to search the counter for. |

<a name="module_CounterAPI..EffectCounter.getCounters"></a>

#### EffectCounter.getCounters(tokenDoc) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
Retreives the array of effect counters of the given token. If the token does not have the flag, creates a single stack counter per effect. Note that the counters are created from data copies, so modifications will not be applied until the counters are updated.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Array.&lt;EffectCounter&gt;</code> - An array of effect counter objects.  

| Param | Type | Description |
| --- | --- | --- |
| tokenDoc | <code>TokenDocument</code> | The token document to fetch the counters for. |

<a name="module_CounterAPI..EffectCounter.getAllCounters"></a>

#### EffectCounter.getAllCounters(token) ⇒ <code>Array.&lt;EffectCounter&gt;</code>
Retreives an array of all simple and active effect counters of the given token. Missing counters are added, but not updated. Note that the counters are created from data copies, so modifications will not be applied until the counters are updated.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Array.&lt;EffectCounter&gt;</code> - An array of (active and regular) effect counter and objects.  

| Param | Type | Description |
| --- | --- | --- |
| token | <code>TokenDocument</code> | The token document to fetch the counters for. |

<a name="module_CounterAPI..EffectCounter.clearEffects"></a>

#### EffectCounter.clearEffects(tokenDoc) ⇒ <code>Promise</code>
Removes all effects from the token. This will also remove permanent active effects from the actor, but retain temporary ones.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the removal update.  

| Param | Type | Description |
| --- | --- | --- |
| tokenDoc | <code>TokenDocument</code> | The token to remove effects from. |

<a name="module_CounterAPI..EffectCounter.redrawCounters"></a>

#### EffectCounter.redrawCounters(token, counters)
Refreshes the given counters for the given token. Note that this will only redraw existing counters and not create any new rendering objects. Because any counter update does this automatically, this function should only be called from custom counter type update logic.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  

| Param | Type | Description |
| --- | --- | --- |
| token | <code>Token</code> | The token document to redraw the counters for. |
| counters | <code>Array.&lt;EffectCounter&gt;</code> | The counters to refresh. |

<a name="module_CounterAPI..EffectCounter.drawCounters"></a>

#### EffectCounter.drawCounters()
Convenience function to draw all counters and refresh the combat UI.

**Kind**: static method of [<code>EffectCounter</code>](#module_CounterAPI..EffectCounter)  
<a name="module_CounterAPI..ActiveEffectCounter"></a>

### CounterAPI~ActiveEffectCounter ⇐ <code>EffectCounter</code>
Represents a counter for a single active effect.

**Kind**: inner class of [<code>CounterAPI</code>](#module_CounterAPI)  
**Extends**: <code>EffectCounter</code>  

* [~ActiveEffectCounter](#module_CounterAPI..ActiveEffectCounter) ⇐ <code>EffectCounter</code>
    * [new ActiveEffectCounter(value, path, parent)](#new_module_CounterAPI..ActiveEffectCounter_new)
    * _instance_
        * [.findParent([parent])](#module_CounterAPI..ActiveEffectCounter+findParent) ⇒ <code>ActiveEffect</code>
        * [.update([parent])](#module_CounterAPI..ActiveEffectCounter+update) ⇒ <code>Promise</code>
        * [.remove([parent])](#module_CounterAPI..ActiveEffectCounter+remove) ⇒ <code>Promise</code>
    * _static_
        * [.findCounter(actor, statusId)](#module_CounterAPI..ActiveEffectCounter.findCounter) ⇒ <code>ActiveEffectCounter</code>
        * [.findCounterValue(actor, statusId)](#module_CounterAPI..ActiveEffectCounter.findCounterValue) ⇒ <code>number</code>
        * [.getCounters(parent)](#module_CounterAPI..ActiveEffectCounter.getCounters) ⇒ <code>Array.&lt;ActiveEffectCounter&gt;</code>

<a name="new_module_CounterAPI..ActiveEffectCounter_new"></a>

#### new ActiveEffectCounter(value, path, parent)
Initializes this counter. If the token does not have an active effect with the given icon path, a temporary effect is created.


| Param | Type | Description |
| --- | --- | --- |
| value | <code>Object</code> \| <code>number</code> | The initial counter value. For numbers, the counter is  initialized as a simple counter. For an object, its properties are  copied to the new EffectCounter and missing values are defaulted. |
| path | <code>string</code> | The icon path of the counter. |
| parent | <code>TokenDocument</code> \| <code>Actor</code> \| <code>ActiveEffect</code> | The token document, actor or effect that the counter is associated with. |

<a name="module_CounterAPI..ActiveEffectCounter+findParent"></a>

#### activeEffectCounter.findParent([parent]) ⇒ <code>ActiveEffect</code>
Resolves the parent entity of this counter. If the parameter is a valid active effect, it is returned immediately. If the parameter is a token document, its active effects are searched for the icon path. Otherwise, both the document and its effects are resolved from the object tree.

**Kind**: instance method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>ActiveEffect</code> - The parent active effect of the counter.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>TokenDocument</code> \| <code>Actor</code> \| <code>ActiveEffect</code> | The parent entity of this counter.  If omitted, it will be resolved using the token ID and the icon path. |

<a name="module_CounterAPI..ActiveEffectCounter+update"></a>

#### activeEffectCounter.update([parent]) ⇒ <code>Promise</code>
Adds or updates this counter from the associated active effect stored in the token's actor.

**Kind**: instance method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..ActiveEffectCounter+remove"></a>

#### activeEffectCounter.remove([parent]) ⇒ <code>Promise</code>
Removes this counter and its effect from its token's actor.

**Kind**: instance method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>Promise</code> - A promise representing the asynchronous operation.  

| Param | Type | Description |
| --- | --- | --- |
| [parent] | <code>ActiveEffect</code> | The parent entity of this counter. |

<a name="module_CounterAPI..ActiveEffectCounter.findCounter"></a>

#### ActiveEffectCounter.findCounter(actor, statusId) ⇒ <code>ActiveEffectCounter</code>
Retreives the counter for the effect with the given ID for the given actor.

**Kind**: static method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>ActiveEffectCounter</code> - The counter object if it exists, undefined otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| actor | <code>Actor</code> | The actor document to search for the effect. |
| statusId | <code>string</code> | The status ID of the effect to search the counter for. |

<a name="module_CounterAPI..ActiveEffectCounter.findCounterValue"></a>

#### ActiveEffectCounter.findCounterValue(actor, statusId) ⇒ <code>number</code>
Retreives the value of the counter for the effect with the given ID for the given actor.

**Kind**: static method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>number</code> - The value of the counter if it exists, undefined otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| actor | <code>Actor</code> | The actor document to search for the effect. |
| statusId | <code>string</code> | The status ID of the effect to search the counter for. |

<a name="module_CounterAPI..ActiveEffectCounter.getCounters"></a>

#### ActiveEffectCounter.getCounters(parent) ⇒ <code>Array.&lt;ActiveEffectCounter&gt;</code>
Retreives the array of active effect counters of the token's actor. If the token does not have an actor, an empty array is returned. For each effect that does not have a counter, a single stack counter is created.

**Kind**: static method of [<code>ActiveEffectCounter</code>](#module_CounterAPI..ActiveEffectCounter)  
**Returns**: <code>Array.&lt;ActiveEffectCounter&gt;</code> - An array of effect counter objects.  

| Param | Type | Description |
| --- | --- | --- |
| parent | <code>TokenDocument</code> \| <code>Actor</code> | The token or actor to fetch the counters for. |

<a name="module_CounterAPI..CounterTypes"></a>

### CounterAPI~CounterTypes
Utility methods for manipulating counter types.

**Kind**: inner class of [<code>CounterAPI</code>](#module_CounterAPI)  

* [~CounterTypes](#module_CounterAPI..CounterTypes)
    * [.types](#module_CounterAPI..CounterTypes.types) ⇒ <code>Object</code>
    * [.defaults](#module_CounterAPI..CounterTypes.defaults) ⇒ <code>Object</code>
    * [.addType(type, [valueGetter], [valueSetter], [allowSelector], [valueDisplay])](#module_CounterAPI..CounterTypes.addType)
    * [.setFont(type, font)](#module_CounterAPI..CounterTypes.setFont)
    * [.setDefaultType(path, type)](#module_CounterAPI..CounterTypes.setDefaultType)

<a name="module_CounterAPI..CounterTypes.types"></a>

#### CounterTypes.types ⇒ <code>Object</code>
Retreives the map of type names and their configurations.

**Kind**: static property of [<code>CounterTypes</code>](#module_CounterAPI..CounterTypes)  
**Returns**: <code>Object</code> - The font, getter, setter and selector for each type.  
<a name="module_CounterAPI..CounterTypes.defaults"></a>

#### CounterTypes.defaults ⇒ <code>Object</code>
Retreives the map of icon paths and their default type names.

**Kind**: static property of [<code>CounterTypes</code>](#module_CounterAPI..CounterTypes)  
**Returns**: <code>Object</code> - The default type for each icon.  
<a name="module_CounterAPI..CounterTypes.addType"></a>

#### CounterTypes.addType(type, [valueGetter], [valueSetter], [allowSelector], [valueDisplay])
Adds a new type and associates a font that counters of that type will be  rendered with.

**Kind**: static method of [<code>CounterTypes</code>](#module_CounterAPI..CounterTypes)  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>String</code> | The fully qualified counter type to apply the font to. This name is  kept internally, but localized for the user interface. |
| [valueGetter] | <code>function</code> | The function to call when accessing the counter.  It should take a single EffectCounter and its parent as input  parameters and return a number. |
| [valueSetter] | <code>function</code> | The function to call when changing the counter.  It should take a single EffectCounter, a number and its parent  as input parameters and return true if it should be updated or null if  it should be removed. |
| [allowSelector] | <code>function</code> | The function to call when determining which types can be selected.  It should take a single EffectCounter and its parent as input parameters  and return true if the type can be selected for that counter.  If no function is provided, the type can always be selected. |
| [valueDisplay] | <code>function</code> | The function to call when displaying the counter.  It should take a single EffectCounter and its parent as input parameters  and return a string. |

<a name="module_CounterAPI..CounterTypes.setFont"></a>

#### CounterTypes.setFont(type, font)
Changes the font for the given counter type.

**Kind**: static method of [<code>CounterTypes</code>](#module_CounterAPI..CounterTypes)  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | The fully qualified counter type to apply the font to. |
| font | <code>PIXI.TextStyle</code> | The PIXI font to use for the type. |

<a name="module_CounterAPI..CounterTypes.setDefaultType"></a>

#### CounterTypes.setDefaultType(path, type)
Changes the default counter type for all effects with the given path. The types of existing counters will not change.

**Kind**: static method of [<code>CounterTypes</code>](#module_CounterAPI..CounterTypes)  

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | The icon path of the effect. |
| type | <code>string</code> | The fully qualified counter type. |

